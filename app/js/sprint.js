var sprint =
{
  sprint: 1500000,
  brake: 180000,

  countdown: (task, time) =>
  {
    if(!time)
    {
      time = sprint.sprint;
    }
    time = time + Date.now();
    sprint.pietimer();
    htmlrender.animation.before(task);
    timer = setInterval(() =>
    {
      now = new Date().getTime();
      target = time - now;
      minutes = Math.floor((target % (1000 * 60 * 60)) / (1000 * 60));
      seconds = Math.floor((target % (1000 * 60)) / 1000);

      progress = minutes + ":" + seconds;

      sprint.display(task, progress);
    }, 1000);
  },

  display: (task, progress) =>
  {
    $("#timer").html(progress);
  },

  animate: (task, duration) =>
  {
    htmlrender.clear();
    sprint.countdown(task);
    sprint.pietimer();
    Materialize.toast('sprint started!', 1000, 'rounded');
    var circle = new ProgressBar.Circle('.countdown',
    {
      color: '#ff3333',
      trailColor: '#757575',
      duration: 15000,
      strokeWidth: 10,
    });

    circle.animate(1, {}, () =>
    {
      $('svg').remove();
      htmlrender.animation.after();
      tasks.display();
      Materialize.toast('sprint completed!', 1000, 'rounded');
      $('#modal_completed').modal('open');
    });

  },

  pietimer: () =>
  {
    $('#pietimer').pietimer(
    {
        seconds: 60,
        color: 'rgba(150, 150, 150, 0.8)',
        height: 60,
        width: 60
    }, () => { $('#pietimer').html('')})
    $('#pietimer').pietimer('start');

    window.setInterval(() =>
    {
      $('#pietimer').pietimer(
      {
        seconds: 60,
        color: 'rgba(255, 255, 255, 0.8)',
        height: 60,
        width: 60
      }, () => { $('#pietimer').html('')})
      $('#pietimer').pietimer('start');
    }, 60100);
  }
};
