const storage = require('electron-json-storage');
const date = require("pretty-date");

const tasks =
{
  sanitize: (input) =>
  {
    return input
      .replace(/&/g, " ")
      .replace(/</g, " ")
      .replace(/>/g, " ")
      .replace(/"/g, " ")
      .replace(/'/g, " ");
  },

  store: (prevkey, status) =>
  {
    if(prevkey)
    {
      storage.has(prevkey, (error, hasKey) =>
      {
        if(hasKey)
        {
          storage.remove(prevkey, (error) =>
          {
            if (error) throw error;
          });
        }
      });
      Materialize.toast('task updated', 1000, 'rounded');
    }

    title = document.getElementById('title').value;
    if(!status)
      status = 'pending';

    if(title || title !== '')
    {
      var task = {
          key: 'task' + Date.now(),
          title: tasks.sanitize(title),
          timestamp: Date.now(),
          status: status,
          archived: false
        };

      storage.set(task.key, task, (error) =>
      {
        if(error) throw error;
      });
      document.getElementById('title').value = '';
      tasks.display();
      Materialize.toast('task added!', 1000, 'rounded');
    }
  },

  display: (status) =>
  {
    count = 0;
    storage.keys((error, keys) =>
    {
      if (error) throw error;
      keys.sort();

      if(tasks.size(keys) == 0)
      {
        htmlrender.emptyList();
      }
      else
      {
        htmlrender.clear();
        for (var key of keys)
        {
          storage.get(key, (error, data) =>
          {
            if (error) throw error;
            if(!data.display) {tasks.filter(data, status)}
          });
        tasks.checkCount();
        }
      }
    });
  },

  filter: (data, status) =>
  {
    if(!status && data.archived == false)
    {
      if(data.archived == false)
      {
        htmlrender.main(data);
        count++;
      }
    }
    else if(status)
    {
      if(data.archived != false)
      {
        htmlrender.main(data);
        count++;
      }
    }
  },

  updateTimeStamp: () =>
  {
    times = document.querySelectorAll('.timestamp');

    for(var i = 0; i < times.length; i++)
    {
      times[i].innerHTML = date.format(new Date(parseInt(times[i].id)));
    }
  },

  edit: (key) =>
  {
    storage.get(key, (error, data) =>
    {
      if (error) throw error;
      $('#modal_add').modal('open');
      document.getElementById('title').value = data.title;
      $('#title').focus();
    });
  },

  redirect: (id) =>
  {
    alert(__dirname);
  },

  delete: (key) =>
  {
    storage.remove(key, (error, data) =>
    {
        if(error) throw error;
    });

    $('#card'+key).remove();
    Materialize.toast('task deleted!', 1000, 'rounded');
  },

  clear: () =>
  {
    storage.clear((error) =>
    {
      if (error) throw error;
    });
    tasks.display();
    $('modal_clear').modal('close');
    Materialize.toast('tasks deleted', 1000, 'rounded');
  },

  sprint: (id) =>
  {
    storage.get(id, (error, data) =>
    {
      if (error) throw error;
      sprint.animate(data);
    });
  },

  archive: (id) =>
  {
    data = storage.get(id, (error, data) =>
    {
      if (error) throw error;
      data.archived = Date.now();
      storage.set(data.key, data, (error) =>
      {
        if(error) throw error;
      });
      $(`#card${id}`).remove();
      Materialize.toast('task archived!', 1000, 'rounded');
    });
  },

  complete: (id) =>
  {
    $('#modal_completed').modal('close');
    storage.get(id, (error, data) =>
    {
      if (error) throw error;
      data.status = 'completed';
      storage.set(data.key, data, (error) =>
      {
        if(error) throw error;
      });
      Materialize.toast('task completed!', 1000, 'rounded');
      $(`#card${id}`).remove();
      htmlrender.main(data);
    });
  },

  datalist: () =>
  {
    storage.getAll((error, data) =>
    {
      if(error) throw error;
      delete data.display;
      $('#datalistinput').val('');
      $.each(data, (key, value) =>
      {
        $('#tasksDatalist').append(
        `<option id=${value.key} value='${value.title}'>${value.title}</option>`);
      });
    });
  },

  size: (data) =>
  {
    size = 0;
    for(key in data)
    {
      if (data.hasOwnProperty(key)) size++;
    }

    if(size == 0) return size;
  },

  checkCount: () =>
  {
    count = $('#task-container').html();
    if(count == '')
    {
      htmlrender.emptyList();
    }
  },

  events: () =>
  {
    $( document ).click((e) => {
      id = e.originalEvent.srcElement.parentNode.id;
      if(id | id != '')
      {
        switch (e.originalEvent.srcElement.innerText)
        {
          case 'edit':
            tasks.edit(id);
            break;
          case 'delete':
            tasks.delete(id);
            break;
          case 'edit':
            tasks.edit(id);
            break;
          case 'access_time':
            tasks.sprint(id);
            break;
          case 'archive':
            tasks.archive(id);
            break;
          case 'done':
            tasks.complete(id);
            break;
          default:
            console.log(e.originalEvent.srcElement.innerText);
        }
      }
    });
  },
};
