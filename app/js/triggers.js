document.getElementById('clear').addEventListener('click',
tasks.clear, false);

document.documentElement.addEventListener('keypress', function(e)
{
  var key = e.which || e.keyCode;
  console.log(key);
  if(key == 59 && document.activeElement != document.getElementById('datalistinput')
    && document.activeElement != document.getElementById('title'))
  {
    $('#modal_sprint').modal('open');
    $('#datalistinput').focus();
    dataArray = tasks.datalist();

  }
  else if(key >= 48 && key <= 122
      && document.activeElement != document.getElementById('datalistinput'))
  {
    $('#modal_add').modal('open');
    document.getElementById('title').focus();
  }
  });

document.getElementById('title').addEventListener('keypress', function (e) {
  var key = e.which || e.keyCode;
  if (key === 13)
  {
    tasks.store();
    $('#modal_add').modal('close');
  }
});

document.getElementById('add').addEventListener('keypress', function (e) {
  var key = e.which || e.keyCode;
  if (key === 13)
  {
    tasks.store();
    $('#modal_add').modal('close');
  }
});

document.querySelector('#views').addEventListener('click', function(e)
{
  settings.changeDisplay(e.target.id);
});

document.querySelector('#status').addEventListener('click', function(e)
{
  tasks.display(e.target.id);
});

document.getElementById('datalistinput').addEventListener('keypress', (e) =>
{
  var key = e.which || e.keyCode;
  if(key === 13)
  {
    tasks.sprint($('#tasksDatalist option')[0].id);
    $('#modal_sprint').modal('close');
  }
});

$('.modal').modal({
  complete: () => { $('#title').val(''); }
});

$('#modal_clear').modal({
  ready: function() {
      document.getElementById('clear').focus();
    },
  }
);

$('.dropdown-button').dropdown(
  {
    inDuration: 300,
    outDuration: 500,
    constrainWidth: false,
    hover: true,
    belowOrigin: true,
    alignment: 'right',
    stopPropagation: false
  }
);

window.setInterval(function(){
  tasks.updateTimeStamp();
}, 10000);

$(document).ready(function() {
  $('select').material_select();
});

$("#title").keyup(function(){
 $("#count").text(`${140 - $(this).val().length} left`);
});
